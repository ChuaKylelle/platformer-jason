using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PushButtonSwitch : MonoBehaviour
{
    [SerializeField] int _playerNumber;
    [SerializeField] Sprite _pressedSprite;
    [SerializeField] UnityEvent _onPressed;
    [SerializeField] UnityEvent _onReleased;
    [SerializeField] float _toggleTimerDelay = 3f;


    bool isPressed;
    SpriteRenderer _spriteRenderer;
    AudioSource _audioSource;
    Sprite _releasedSprite;
    

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _audioSource = GetComponent<AudioSource>();
        _releasedSprite = _spriteRenderer.sprite;
        BecomeReleased();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if (player == null || player.PlayerNumber != _playerNumber) { return; }

        BecomePressed();
    }

    void BecomePressed()
    {
        if(!isPressed)
        {
            isPressed = true;
            _spriteRenderer.sprite = _pressedSprite;

            if(_audioSource != null)
                _audioSource.Play();

            _onPressed?.Invoke();
        }
       
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if (player == null || player.PlayerNumber != _playerNumber) { return; }

        if(_onReleased.GetPersistentEventCount() > 0)
            DelayedReleased();

        
    }

    void BecomeReleased()
    {
        _spriteRenderer.sprite = _releasedSprite;

        _onReleased?.Invoke();

        //StartCoroutine(ToggleDelay());
    }

    void DelayedReleased()
    {
        _spriteRenderer.sprite = _releasedSprite;

        StartCoroutine(ToggleDelay());
    }

    IEnumerator ToggleDelay()
    {

        yield return new WaitForSeconds(_toggleTimerDelay);

        _onReleased?.Invoke();
    }

}
