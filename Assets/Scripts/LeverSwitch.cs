using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LeverSwitch : MonoBehaviour
{
    [SerializeField] List<AudioClip> _clip;

    [SerializeField] LeverDirection _startingDirection = LeverDirection.Center;

    [SerializeField] Sprite _leftSwitchSprite;
    [SerializeField] Sprite _rightSwitchSprite;
    [SerializeField] Sprite _centerSwitchSprite;

    [SerializeField] UnityEvent _onLeftSwitch;
    [SerializeField] UnityEvent _onRightSwitch;
    [SerializeField] UnityEvent _onCenterSwitch;

    SpriteRenderer _spriteRenderer;
    AudioSource _audioSource;
    LeverDirection _currentDirection;

    enum LeverDirection
    {
        Left,
        Center,
        Right
    };

    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        SetLeverDirection(_startingDirection, true);
        
    }

    //If you want collision with the player
    void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.collider.GetComponent<Player>();
        if (player == null) return;

        if(collision.contacts[0].normal.x < 0)
            LeftSwitchActivate();

        if (collision.contacts[0].normal.x > 0)
            RightSwitchActivate();
    }

    //If you dont want collision with the player
    void OnTriggerStay2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if (player == null) return;

        var playerRigidbody = collision.GetComponent<Rigidbody2D>();
        if (playerRigidbody == null) return;

        bool wasOnRight = collision.transform.position.x > transform.position.x;
        bool playerWalkingRight = playerRigidbody.velocity.x > 0;
        bool playerWalkingLeft = playerRigidbody.velocity.x < 0;

        if (wasOnRight && playerWalkingRight)
            SetLeverDirection(LeverDirection.Right);
        else if (wasOnRight == false && playerWalkingLeft)
            SetLeverDirection(LeverDirection.Left);
            

        //if (GetLeverAndPlayerDirection(player) > 0)
        //    LeftSwitchActivate();

        //if (GetLeverAndPlayerDirection(player) < 0)
        //    RightSwitchActivate();
    }

    void SetLeverDirection(LeverDirection direction, bool force = false)
    {
        if (force == false && _currentDirection == direction) return;

        PlayAudio();

        _currentDirection = direction;

        switch (direction)
        {
            case LeverDirection.Right:
                {
                    _spriteRenderer.sprite = _rightSwitchSprite;
                    _onRightSwitch.Invoke();
                    break;
                }
            case LeverDirection.Center:
                {
                    _spriteRenderer.sprite = _centerSwitchSprite;
                    _onCenterSwitch.Invoke();
                    break;
                }
            case LeverDirection.Left:
                {
                    _spriteRenderer.sprite = _leftSwitchSprite;
                    _onLeftSwitch.Invoke();
                    break;
                }
            default:
                break;
        }
    }

    private void PlayAudio()
    {
        if(_audioSource != null)
        {
            var randomIndex = UnityEngine.Random.Range(0, _clip.Count);
            AudioClip clip = _clip[randomIndex];

            _audioSource.PlayOneShot(clip, 1f);
        }
        
    }

    float GetLeverAndPlayerDirection(Player player)
    {
        return (this.transform.position - player.transform.position).normalized.x;
    }

    void RightSwitchActivate()
    {
        //_spriteRenderer.sprite = _rightSwitchSprite;
        _onRightSwitch.Invoke();
    }

    void LeftSwitchActivate()
    {
        //_spriteRenderer.sprite = _leftSwitchSprite;
        _onLeftSwitch.Invoke();
    }

    public void DebugMessage()
    {
        Debug.Log("testing UnityEvent");
    }

    void OnValidate()
    {
        switch (_startingDirection)
        {
            case LeverDirection.Right:
                {
                    GetComponent<SpriteRenderer>().sprite = _rightSwitchSprite;
                    
                    break;
                }
            case LeverDirection.Center:
                {
                    GetComponent<SpriteRenderer>().sprite = _centerSwitchSprite;
                    
                    break;
                }
            case LeverDirection.Left:
                {
                    GetComponent<SpriteRenderer>().sprite = _leftSwitchSprite;
                   
                    break;
                }
            default:
                break;
        }
    }
}
