using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] int _playerNumber = 1;
    [Header("Movement")]
    [SerializeField] float _speed = 20f;
    [SerializeField] float _slipFactor = 1;
    [Header("Jump")]
    [SerializeField] float _jumpVelocity = 2f;
    [SerializeField] float _downPull = 5f;
    [SerializeField] float _wallSildeSpeed = 1f;
    [SerializeField] float maxJumpDuration = 0.1f;
    [SerializeField] float _acceleration = 1f;
    [SerializeField] float _breaking = 1f;
    [SerializeField] float _airAcceleration = 1f;
    [SerializeField] float _airBreaking = 1f;
    [SerializeField] int _maxJump = 2;
    [SerializeField] Transform _feet;
    [SerializeField] Transform _leftSensor;
    [SerializeField] Transform _rightSensor;
    


    Vector2 _startingPosition;
    int _jumpCount;
    float _fallTimer;
    float _jumpTimer;

    Rigidbody2D _rb;
    Animator _anim;
    SpriteRenderer _spriteRenderer;
    AudioSource _audioSource;
    bool _isGrounded;
    bool _isOnSlipperySurface;
    float _horizontal;
    string _jumpButton;
    string _horizontalButton;
    int _layerMask;
    

    public int PlayerNumber => _playerNumber;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _anim = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    

    void Start()
    {
        _jumpButton = $"P{_playerNumber}Jump";
        _horizontalButton = $"P{_playerNumber}Horizontal";
        _layerMask = LayerMask.GetMask("Default");

        _startingPosition = transform.position;
        _jumpCount = _maxJump;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateIsGrounded();
        ReadHorizontalInput();

        if (_isOnSlipperySurface)
            SlipHorizontal();
        else
            MoveHorizontal();
       
        AnimatePlayer();

        CharacterFlipDirection();

        if (ShouldSlide())
        {
            if (ShouldStartJump())
                WallJump();
            else
                Slide();
            return;
        }
            

        if (ShouldStartJump())
            Jump();
        else if (ShouldContinueJump())
            ContinueJump();

        _jumpTimer += Time.deltaTime;

        if (_isGrounded && _fallTimer > 0)
        {
            _fallTimer = 0;
            _jumpCount = _maxJump;
        }
        else
        {
            _fallTimer += Time.deltaTime;
            var downForce = _downPull * _fallTimer * _fallTimer;
            _rb.velocity = new Vector2(_rb.velocity.x, _rb.velocity.y - downForce);
        }

    }

    private void WallJump()
    {
        _rb.velocity = new Vector2(-_horizontal * _jumpVelocity, _jumpVelocity * 1.5f);
    }

    void Slide()
    {
        _rb.velocity = new Vector2(_rb.velocity.x, -_wallSildeSpeed);
    }

    bool ShouldSlide()
    {
        if (_isGrounded)
            return false;

        if (_rb.velocity.y > 0)
            return false;

        if(_horizontal < 0)
        {
            var hit = Physics2D.OverlapCircle(_leftSensor.position, 0.1f);
            if(hit != null && hit.CompareTag("Wall"))
                return true;
        }

        if (_horizontal > 0)
        {
            var hit = Physics2D.OverlapCircle(_rightSensor.position, 0.1f);
            if (hit != null && hit.CompareTag("Wall"))
                return true;
        }

        return false;
    }

    void ContinueJump()
    {
        _rb.velocity = new Vector2(_rb.velocity.x, _jumpVelocity);
        _fallTimer = 0;
    }

    void Jump()
    {
        _rb.velocity = new Vector2(_rb.velocity.x, _jumpVelocity);
        _jumpCount--;
        Debug.Log($"Jumps Remaining {_jumpCount}");
        _fallTimer = 0;
        _jumpTimer = 0;

        if(_audioSource != null)
            _audioSource.Play();
    }

    bool ShouldContinueJump()
    {
        return Input.GetButton(_jumpButton) && _jumpTimer <= maxJumpDuration;
    }

    void UpdateIsGrounded()
    {
        var hit = Physics2D.OverlapCircle(_feet.position, 0.1f, _layerMask);
        _isGrounded = hit != null;

        _isOnSlipperySurface = hit?.CompareTag("Slippery") ?? false;

        //Longer Version of this ^
        //if (hit != null)
        //    _isOnSlipperySurface = hit.CompareTag("Slippery");
        //else
        //    _isOnSlipperySurface = false;
        
       
    }

    bool ShouldStartJump()
    {
        return Input.GetButtonDown(_jumpButton) && _jumpCount > 0;
    }

    void AnimatePlayer()
    {
        bool walking = _horizontal != 0;
        _anim.SetBool("Walk", walking);
        _anim.SetBool("Jump", ShouldContinueJump());
        _anim.SetBool("Slide", ShouldSlide());
    }

    void MoveHorizontal()
    {
        float smoothnessMultiplier = _horizontal == 0 ? _breaking : _acceleration;

        if (_isGrounded == false)
            smoothnessMultiplier = _horizontal == 0 ? _airBreaking : _airAcceleration;

        var newHorizontal = Mathf.Lerp(_rb.velocity.x, 
                            _horizontal * _speed, 
                            Time.deltaTime * smoothnessMultiplier);

        _rb.velocity = new Vector2(newHorizontal, _rb.velocity.y);
    }

    void SlipHorizontal()
    {
        var desiredVelocity = new Vector2(_horizontal, _rb.velocity.y);
        var smoothVelocity = Vector2.Lerp(
            _rb.velocity, 
            desiredVelocity, 
            Time.deltaTime / _slipFactor);

        _rb.velocity = smoothVelocity;
    }

    void ReadHorizontalInput()
    {
        _horizontal = Input.GetAxis(_horizontalButton);
    }

    void CharacterFlipDirection()
    {
        if (_horizontal != 0)
        {            
            _spriteRenderer.flipX = _horizontal < 0;
        }
    }

    internal void ResetPosition()
    {
        _rb.position = _startingPosition;
        SceneManager.LoadScene("Menu");
    }

    internal void TeleportTo(Vector3 position)
    {
        _rb.position = position;
        _rb.velocity = Vector2.zero;
    }


}
