using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] float _launchForce = 8f;
    [SerializeField] float _bounceForce = 5f;
    [SerializeField] int _bounceLimit = 3;

    Rigidbody2D _rigidbody;
    

    public float Direction { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.velocity = new Vector2(_launchForce * Direction, 0);
        //_rigidbody.AddForce(Vector2.right * _launchForce);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ITakeDamage damagable = collision.collider.GetComponent<ITakeDamage>();
        if(damagable != null)
        {
            damagable.TakeDamage();
            Destroy(gameObject);
            return;
        }

        _bounceLimit--;
        if(_bounceLimit < 0)
            Destroy(gameObject);
        //else
        //    _rigidbody.velocity = new Vector2(_launchForce * Direction, _bounceForce);
    }
}
