using System;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    public static event Action<int> OnScoreChanged;
    public static event Action<int> OnHighScoreChanged;

    public static int Score { get; private set; }
    static int _highScore = 0;

    public static int HighScore => _highScore;

    void Start()
    {
        string key = "HighScore";

        _highScore = PlayerPrefs.GetInt(key, _highScore);
        Score = 0;
    }

    public static void Add(int points)
    {
        Score += points;
        OnScoreChanged?.Invoke(Score);

        if (Score > _highScore)
        {
            _highScore = Score;
            OnHighScoreChanged?.Invoke(_highScore);
        }
            

        Debug.Log($"Score: {Score}");
    }

    public static void SetNewHighScore()
    {
        string key = "HighScore";
        PlayerPrefs.SetInt(key, _highScore);
    }

   
}
