using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Flag : MonoBehaviour
{
    [SerializeField] string _sceneName;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if(player == null) { return; }

        var anim = GetComponent<Animator>();
        anim.SetTrigger("Raise");
        StartCoroutine(FlagTransition()); 

       
        

    }


    private IEnumerator FlagTransition()
    {
        var audioSource = GetComponent<AudioSource>();
        if(audioSource != null)
            audioSource.Play();
        PlayerPrefs.SetInt(_sceneName + "Unlocked", 1);
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(_sceneName);
    }
}
