using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIStartLevelButton : MonoBehaviour
{
    [SerializeField] string _levelName;

    public string LevelName => _levelName; // expression body property

    public void LoadLevel()
    {
        SceneManager.LoadScene(_levelName);
    }

   
   
}

