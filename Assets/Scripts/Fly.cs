using UnityEngine;

public class Fly : MonoBehaviour, ITakeDamage
{
    
    Vector2 _orginalPosition;
    [SerializeField] Vector2 _direction = Vector2.up;
    [SerializeField] float _maxDistance = 2f;
    [SerializeField] float _speed = 2f;

    public void TakeDamage()
    {
        gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        _orginalPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(_direction.normalized * Time.deltaTime * _speed);
        var distance = Vector3.Distance(_orginalPosition, transform.position);

        if(distance >= _maxDistance)
        {
            transform.position = _orginalPosition + (_direction.normalized * _maxDistance);
            _direction *= -1;
        }
    }


}
