using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour, ITakeDamage
{
    [SerializeField] List<AudioClip> _clip;

    public void TakeDamage()
    {
        TakeHit();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.collider.GetComponent<Player>() == null) { return; }

        if (collision.contacts[0].normal.y > 0)
        {
            TakeHit();
        }



    }

    private void TakeHit()
    {
        var particleSystem = GetComponent<ParticleSystem>();
        particleSystem.Play();

        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

        var randomIndex = UnityEngine.Random.Range(0, _clip.Count);
        AudioClip clip = _clip[randomIndex];

        GetComponent<AudioSource>().PlayOneShot(clip, 1f);



    }
}
