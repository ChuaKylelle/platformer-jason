using UnityEngine;

public class ItemBox : IsHittableFromBelow
{
    [SerializeField] GameObject _itemPrefab;
    [SerializeField] GameObject _item;
    [SerializeField] Vector2 _itemLaunchVelocity;

    bool _used;

    protected override bool CanUse => _used == false;

    // Start is called before the first frame update
    void Start()
    {
        if(_item != null)
            _item.SetActive(false);
    }

    protected override void Use()
    {
        _item = Instantiate(
            _itemPrefab,
            transform.position + Vector3.up, 
            Quaternion.identity, 
            transform);

        if (_item == null)
            return;

        _used = true;
        _item.SetActive(true);

        var itemRigidBody = _item.GetComponent<Rigidbody2D>();
        if (itemRigidBody != null)
        {
            itemRigidBody.velocity = _itemLaunchVelocity;
        }

    }


}
