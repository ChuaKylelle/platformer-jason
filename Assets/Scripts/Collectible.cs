﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class Collectible : MonoBehaviour
{

    public event Action OnPickedUp;
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();

        if(player == null) { return; }

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<PolygonCollider2D>().enabled = false;

        var audioSource = GetComponent<AudioSource>();
        if(audioSource != null)
            GetComponent<AudioSource>().Play();

        OnPickedUp?.Invoke();

        
    }


}