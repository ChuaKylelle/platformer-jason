using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] List<AudioClip> _coinAudio;

    public static int CoinsCollected;

    void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.GetComponent<Player>();
        if(player == null) { return; }

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        CoinsCollected++;
        ScoreSystem.Add(100);

        if(_coinAudio.Count > 0)
        {
            var random = UnityEngine.Random.Range(0, _coinAudio.Count);
            AudioClip clip = _coinAudio[random];
            GetComponent<AudioSource>().PlayOneShot(clip, 1f);
        }
        else
        {
            GetComponent<AudioSource>().Play();
        }

    }
}
