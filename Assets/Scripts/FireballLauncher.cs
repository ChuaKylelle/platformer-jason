using UnityEngine;

public class FireballLauncher : MonoBehaviour
{
    [SerializeField] Fireball _fireballPrefab;
    [SerializeField] float _fireRate= 1f;

    //bool _canShoot = true;
    Player _player;
    string _fireButton;
    string _horizontalButton;
    float _nextFireTime;

    // Start is called before the first frame update
    void Awake()
    {
        _player = GetComponent<Player>();
        _fireButton = $"P{_player.PlayerNumber}Fire";
        _horizontalButton = $"P{_player.PlayerNumber}Horizontal";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown(_fireButton) && Time.time >= _nextFireTime)
        {
            var horizontal = Input.GetAxis(_horizontalButton);
            Fireball fireball = Instantiate(_fireballPrefab, transform.position, Quaternion.identity);
            fireball.Direction = horizontal >= 0 ? 1f : -1f;
            _nextFireTime = Time.time + _fireRate;
            //StartCooldown();
        }
    }

    //void StartCooldown()
    //{
    //    StartCoroutine(FireballCooldown());
    //}

    //IEnumerator FireballCooldown()
    //{
    //    _canShoot = false;
    //    yield return new WaitForSeconds(_fireRate);
    //    _canShoot = true;
    //}
}
