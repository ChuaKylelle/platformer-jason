using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    [SerializeField] List<AudioClip> _clip;
    [SerializeField] float _bounceVelocity = 7f;

    AudioSource _audioSource;

    void Awake() => _audioSource = GetComponent<AudioSource>();
    

    void OnCollisionEnter2D(Collision2D collision)
    {
        var player = collision.collider.GetComponent<Player>();

        if(player != null)
        {
            //bounce
            var rb = player.GetComponent<Rigidbody2D>();
            if(rb != null)
            {
                rb.velocity = new Vector2(rb.velocity.x, _bounceVelocity);
                PlayAudio();

            }

        }
    }

    private void PlayAudio()
    {
        
        if (_audioSource != null)
        {
            if (_clip.Count > 0)
            {
                var randomIndex = UnityEngine.Random.Range(0, _clip.Count);
                AudioClip clip = _clip[randomIndex];
                _audioSource.PlayOneShot(clip, 1);
            }
            else
            {
                _audioSource.Play();
            }

        }
    }
}
