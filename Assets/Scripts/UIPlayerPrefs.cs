using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPlayerPrefs : MonoBehaviour
{
    [SerializeField] string _key;

    private void OnEnable()
    {
        var value = PlayerPrefs.GetInt(_key);
        GetComponent<TMP_Text>().SetText("HighScore: " + value.ToString());
    }

    [ContextMenu("Clear HighScore")]
    void ClearHighScore()
    {
        PlayerPrefs.DeleteKey(_key);
    }
}
