﻿using UnityEngine;
using TMPro;

public class UIHighScore : MonoBehaviour
{
    TMP_Text _text;

    void Start()
    {
        _text = GetComponent<TMP_Text>();
        _text.SetText(ScoreSystem.HighScore.ToString());
        ScoreSystem.OnHighScoreChanged += UpdateHighScoreText;
    }

    void OnDestroy()
    {
        ScoreSystem.OnHighScoreChanged -= UpdateHighScoreText;
    }

    void UpdateHighScoreText(int highScore)
    {
        
        _text.SetText(highScore.ToString());
        ScoreSystem.SetNewHighScore();
    }


}

